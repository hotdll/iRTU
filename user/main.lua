--必须在这个位置定义PROJECT和VERSION变量
--PROJECT：ascii string类型，可以随便定义，只要不使用,就行
--VERSION：ascii string类型，如果使用Luat物联云平台固件升级的功能，必须按照"X.X.X"定义，X表示1位数字；否则可随便定义
PROJECT = "iRTU"
-- PROJECT = "DTU-AIR720-MODUL"
VERSION = "2.0.1"
-- PRODUCT_KEY = "EGD7WPh2oxbWgDNhCgLRazQjUzbS5U1R"--beta版本的key
PRODUCT_KEY = "DPVrZXiffhEUBeHOUwOKTlESam3aXvnR"--正式版本的key
-- PRODUCT_KEY = "UFsrnKt6g3h1IKV7ryKkpZnGrjnPg0xR"--黄何测试key

-- PRODUCT_KEY = "YoR8ob2ELJOczNrVOZZTtAcmLAf6eyYw" --用户DTU-AIR720-MODUL 更新KEY
--加载日志功能模块，并且设置日志输出等级
--如果关闭调用log模块接口输出的日志，等级设置为log.LOG_SILENT即可
require "log"
-- LOG_LEVEL = log.LOGLEVEL_INFO
LOG_LEVEL = log.LOGLEVEL_TRACE
require "sys"
require "net"
require "utils"
require "patch"
require"config"
require "nvm"

-- nvm.init("config.lua")
-- 系统变量
ver = rtos.get_version():upper()
is8955 = ver:find("8955")
is8910 = ver:find("8910")
is1802 = ver:find("1802")
is1802S = ver:find("1802S")
isTTS = ver:find("TTS") or ver:find("8955F")
isSSL = ver:find("SSL") or ver:find("8955F") or ver:find("1802") or ver:find("8910")
if is8910 then pmd.init({}) end
--每1分钟查询一次GSM信号强度
--每1分钟查询一次基站信息
net.startQueryAll(8000, 60000)

--此处关闭RNDIS网卡功能
--否则，模块通过USB连接电脑后，会在电脑的网络适配器中枚举一个RNDIS网卡，电脑默认使用此网卡上网，导致模块使用的sim卡流量流失
--如果项目中需要打开此功能，把ril.request("AT+RNDISCALL=0,1")修改为ril.request("AT+RNDISCALL=1,1")即可
--注意：core固件：V0030以及之后的版本、V3028以及之后的版本，才以稳定地支持此功能
-- ril.request("AT+RNDISCALL=0,1")

if rtos.get_version():upper():find("ASR1802") then
    ril.request("AT+MEDCR=0,8,1")
    ril.request("AT+MEDCR=0,17,240")
    ril.request("AT+MEDCR=0,19,1")
    rtos.set_trace_port(2)
elseif rtos.get_version():upper():find("8955") then
    require "wdt"
    wdt.setup(pio.P0_30, pio.P0_31)
end
-- 加载NTP,错误日志，远程升级功能库
require "ntp"
require "errDump"
require "update"
--加载主程序
require "default"

--启动系统框架
sys.init(0, 0)
sys.run()
